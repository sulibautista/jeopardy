'use strict';

var fs = require('fs'),
    path = require('path'),
    XLSX = require('xlsx'),
    _ = require('lodash');

var book = XLSX.readFile(path.join(__dirname, 'jeopardy.xls') || process.argv[2]),
    firstSheetName = book.SheetNames[0],
    firstSheet = book.Sheets[firstSheetName],
    data = XLSX.utils.sheet_to_json(firstSheet),
    out = [],
    rows = 5,
    cols = 5;

_.forOwn(data[0], function(v, topic){
    out.push({
        topic: topic,
        clues: _.range(rows).map(function () { return {}; })
    });
});

var i = 0,
    k = -1,
    currProp,
    props = ['prize', 'question', 'answer'];

data.forEach(function(row){
    if(++k % rows === 0){
        currProp = props[Math.floor(k / rows)];
    }

    _.forOwn(row, function(val){
        out[i++ % cols].clues[k % rows][currProp] = currProp === 'prize' ? parseInt(val) : (_.trim(val) === '' ? null : _.trim(val));
    });
});

fs.writeFileSync(path.join(__dirname, 'jeopardy.json'), JSON.stringify(out, null, 2));
