"use strict";

var _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    express = require('express');

var io = require('socket.io')(7171),
    webServer = express();

var sessionID = 1,
    stateChangeCounter = 0;

try {
    var session = JSON.parse(fs.readFileSync('.jeopardySession', 'utf8'));
    sessionID = session.id;
    stateChangeCounter = session.stateChangeCounter;
} catch(e) {
    if(e.code !== 'ENOENT') { console.error('Error reading session file: %s', e); }
    writeSession();
}

webServer.use(express.static(path.join(__dirname, '/dist')));
webServer.listen(8080);

ensureSessionFolder();

console.log('Starting/continuing session ID ' + sessionID + ' with state count=%d on directory ' + getSessionDir(), stateChangeCounter);

io.on('connection', function(socket){
    console.log('Client %s connected.', socket.id);

    socket.on('OpenSession', function(){
        ++sessionID;
        stateChangeCounter = 0;
        ensureSessionFolder();
        console.log("[OpenSession] Client id %s", socket.id);
    });

    socket.on('UpdateState', function(msg){
        io.emit('StateUpdated', msg);
        fs.writeFile(getSessionDir() + '/' + (++stateChangeCounter) + '.json', JSON.stringify(msg, null, 2), 'utf8', function(err) {
            if (err) { console.error("Error writing state update to disk: %s", err); }
        });
        writeSession();
        console.log("[UpdateState] Client id %s", socket.id);
    });

    socket.on('GetState', function(){
        fs.readFile(getSessionDir() + '/' + stateChangeCounter + '.json', 'utf8', function(err, json){
            var data = null;
            try {
                if (!err) {
                    data = JSON.parse(json);
                }
            } catch(e) {
                err = e;
            } finally {
                if(err) {
                    console.error('[GetState] Error reading status file %s: %s', stateChangeCounter, err);
                } else {
                    io.to(socket.id).emit('StateUpdated', data);
                }
            }
        });
        console.log("[GetState] Client id %s", socket.id);
    });

    socket.on('GetStateList', function(){
        var sessionDir = getSessionDir();

        fs.readdir(sessionDir, function(err1, files){
            if (err1) {
                return;
            }

            var c = 0,
                data = new Array(files.length);

            files.sort(function (a, b) {
                return parseInt(a, 10) - parseInt(b, 10);
            })
            .forEach(function(file){
                var idx = c++;
                fs.readFile(sessionDir + '/' + file, 'utf-8', function(err, json){
                    try{
                        if (!err) {
                            data[idx] = JSON.parse(json);
                        }

                    } catch(e) {
                        err = e;
                    } finally {
                        if(err) {
                            console.error('[GetStateList] Error reading status file %s: %s', file, err);
                        }
                    }

                    if (--c === 0) {
                        io.to(socket.id).emit('StateList', _.compact(data));
                    }
                });
            });
        });
        console.log("[GetStateList] Client id %s", socket.id);
    });

    socket.on('disconnect', function(){
        console.log('Client %s disconnected.', socket.id);
    });
});

function getSessionDir() {
    return 's_' + sessionID;
}

function ensureSessionFolder() {
    try {
        fs.mkdirSync(getSessionDir());
    } catch(e) {
        if (e.code !== 'EEXIST') { throw e; }
    }
}

function writeSession() {
    var sess = {id: sessionID, stateChangeCounter: stateChangeCounter};
    fs.writeFile('.jeopardySession', JSON.stringify(sess), 'utf8', function(err){
        if(err) { console.error('Error writing session file: %s', err); }
    });
}
