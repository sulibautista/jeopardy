'use strict';

module.exports = function (grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({

        less: {
            global: {
                files: {
                    'dist/css/global.min.css': 'src/less/global.less'
                }
            },
            display: {
                files: {
                    'dist/css/display.min.css': 'src/less/display.less'
                }
            },
            console: {
                files: {
                    'dist/css/console.min.css': 'src/less/console.less'
                }
            }
        },

        autoprefixer: {
            options: {
                browsers: ['last 2 versions']
            },
            all: {
               files: {
                    'dist/css/console.min.css': 'dist/css/console.min.css',
                    'dist/css/display.min.css': 'dist/css/display.min.css',
                    'dist/css/global.min.css': 'dist/css/global.min.css'
               }
            }
        },

        browserify: {
            options: {
                watch: true,
                debug: true,
                transform: [
                    'babelify'
                ]
            },
            display: {
                files: {
                    'dist/js/display.js': 'src/display.js'
                }
            },
            console: {
                files: {
                    'dist/js/console.js': 'src/console.js'
                }
            }
        },

        watch: {

            js: {
                files: ['src/**/*.js', 'src/**/*.jsx'],
                tasks: ['browserify']
            },
            less: {
                files: 'src/less/*.*',
                tasks: ['less', 'autoprefixer']
            }
        }
    });

    grunt.registerTask('build', [
        'less',
        'autoprefixer',
        'browserify'
    ]);

    grunt.registerTask('default', [
        'build',
        'watch'
    ]);
};
