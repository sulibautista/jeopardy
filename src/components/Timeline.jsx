"use strict";
import React from "react";
import clueStateTools from '../clue-state-tools.js';

export default React.createClass({
    propTypes: {
        gameStateList: React.PropTypes.arrayOf(
            React.PropTypes.shape({
                clueDefinition: clueStateTools.clueDefinitionType,
                clueState: clueStateTools.clueStateType
            }).isRequired
        ).isRequired,
        snapshopSelected: React.PropTypes.func.isRequired,
        snapshotAccepted: React.PropTypes.func.isRequired
    },
    getInitialState: function(){
        return { selectedIndex: null };
    },
    handleSnapshotClicked: function(i) {
        setTimeout(this.props.snapshopSelected.bind(null, i), 0);
        this.setState({ selectedIndex: i });
    },
    handleRestore: function(){
        this.props.snapshotAccepted();
    },
    componentWillReceiveProps: function(nextProps) {
        if(this.props.gameStateList.length !== nextProps.gameStateList.length) {
            this.handleSnapshotClicked(nextProps.gameStateList.length - 1);
        }
    },
    render: function() {
        let selected = this.state.selectedIndex,
            snapshots = this.props.gameStateList.map(function(state, i){
            return (
                <div key={i} className={"snapshot" + (selected === i ? ' selected' : '')} onClick={this.handleSnapshotClicked.bind(this, i)}>
                    {i + 1}
                </div>
            );
        }, this);

        let isCurrent = selected === null || selected === this.props.gameStateList.length - 1;
        this.shouldScrollRight = isCurrent;

        return (
            <div className="timeline cols">
                <div className="snapshot-list">
                    {snapshots}
                </div>
                <div className="timeline-controls rows">
                    <button className="timeline-accept" disabled={isCurrent} onClick={this.handleRestore}>Restore</button>
                </div>
            </div>
        );
    },
    componentDidUpdate: function() {
        if(this.shouldScrollRight) {
            var node = this.getDOMNode().getElementsByClassName('snapshot-list')[0];
            node.scrollLeft = node.scrollWidth;
        }
    }
});
