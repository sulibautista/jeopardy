"use strict";
import React from 'react/addons';
import _ from 'lodash';
import ClueSet from "./ClueSet.jsx";
import TeamScore from "./TeamScore.jsx";
import OverlordScreen from "./OverlordScreen.jsx";
import TimeoutTransitionGroup from './TimeoutTransitionGroup.jsx';
import clueStateTools from '../clue-state-tools.js';

export default React.createClass({
    propTypes: {
        clueDefinition: clueStateTools.clueDefinitionType,
        clueState: clueStateTools.clueStateType,
        selectable: React.PropTypes.bool.isRequired,
        clueSelected: React.PropTypes.func
    },
    getInitialState: function() {
        return { selectedClue: {topic: null, offset: null} };
    },
    componentDidMount() {
        document.addEventListener("keyup", this.handleKeyUp);
    },
    componentWillUnmount() {
        document.removeEventListener("keyup", this.handleKeyUp);
    },
    handleKeyUp: function(e) {
        let y = this.state.selectedClue.topic || 0,
            x = this.state.selectedClue.offset || 0;

        switch(e.which){
            case 37: y--; break;
            case 38: x--; break;
            case 39: y++; break;
            case 40: x++; break;
            default: return;
        }

        e.preventDefault();

        let xMax = this.props.clueState[0].length,
            yMax = this.props.clueState.length;

        y = y < 0 ? yMax - 1 : y % yMax;
        x = x < 0 ? xMax - 1 : x % xMax;

        let sel = { topic: y, offset: x };
        this.setState({ selectedClue: sel});
        this.props.clueSelected(sel);
    },
    handleClueSelected: function(topic, offset){
        let sel = { topic, offset };
        this.setState({ selectedClue: sel });
        this.props.clueSelected(sel);
    },
    render: function() {
        let clueDef = _.cloneDeep(this.props.clueDefinition),
            clueState = this.props.clueState;

        for(let i = 0; i < clueState.length; ++i) {
            for(let k = 0; k < clueState[i].length; ++k) {
                _.extend(clueDef[i].clues[k], clueState[i][k]);
            }
        }

        let clueSet = clueDef.map(function(clueSetData, i){
            return (
               <ClueSet topic={clueSetData.topic}
                        clues={clueSetData.clues}
                        key={i}
                        topicIndex={i}
                        selectable={this.props.selectable}
                        clueSelected={this.handleClueSelected}
                        selectedClue={i === this.state.selectedClue.topic ? this.state.selectedClue.offset : null} />
            );
        }, this);

        let activeClue = clueStateTools.getActive(clueState),
            screen;

        if(activeClue) {
            screen = (<OverlordScreen clueDefinition={this.props.clueDefinition} activeClue={activeClue} key={1} />);
        }

        return (
            <div className="jeopardy">
                <div className="board">
                    <div className="cols">
                        {clueSet}
                    </div>
                    <div className="cols">
                        <TeamScore clueDefinition={this.props.clueDefinition} clueState={clueState} teamId={1} />
                        <TeamScore clueDefinition={this.props.clueDefinition} clueState={clueState} teamId={2} />
                    </div>
                </div>
                <TimeoutTransitionGroup enterTimeout={1000} leaveTimeout={1000} transitionName="slide">
                    {screen}
                </TimeoutTransitionGroup>
            </div>
        );
    }
});
