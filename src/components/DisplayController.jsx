"use strict";
import React from "react";
import Jeopardy from "./Jeopardy.jsx";
import io from 'socket.io-client';

export default React.createClass({
    getInitialState: function() {
        return { clueDefinition: [], clueState: [] };
    },
    componentDidMount: function() {
        let socket = io(window.location.hostname + ':' + 7171);

        socket.on('StateUpdated', function(data) {
            this.setState(data);
        }.bind(this));

        socket.emit('GetState');
    },
    render: function() {
        return (
            <Jeopardy clueDefinition={this.state.clueDefinition} clueState={this.state.clueState} selectable={false}/>
        );
    }
});
