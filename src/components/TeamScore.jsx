"use strict";
import React from 'react';
import clueStateTools from '../clue-state-tools.js';

export default React.createClass({
    propTypes: {
        clueDefinition: clueStateTools.clueDefinitionType,
        clueState: clueStateTools.clueStateType,
        teamId: React.PropTypes.number.isRequired
    },
    render: function(){
        let def = this.props.clueDefinition,
            teamId = this.props.teamId,
            sum = 0;

        clueStateTools.forEach(this.props.clueState, function(clue, topic, offset){
            if(clue.state === 'cashed' && clue.team === teamId){
                sum += (+def[topic].clues[offset].prize) * (+clue.bountyMult);
            }
        });

        return (
            <div className="team-score">
                {sum}
            </div>
        );
    }
});
