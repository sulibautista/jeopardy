"use strict";

import React from "react/addons";
import io from 'socket.io-client';
import _ from 'lodash';
import Jeopardy from "./Jeopardy.jsx";
import Controls from "./Controls.jsx";
import Timeline from "./Timeline.jsx";

let clueDefinition = require('../../jeopardy.json');

let initGameData = {
    clueDefinition,
    clueState: _.range(clueDefinition.length).map(function () { return []; })
};

clueDefinition.forEach(function(clueSet, topic){
    clueSet.clues.forEach(function(){
        initGameData.clueState[topic].push({
            state: 'hidden',
            team: null,
            bountyMult: 1,
            prevState: 'hidden',
            activeState: null
        });
    });
});

export default React.createClass({
    socket: null,
    getInitialState: function() {
        return { game: initGameData, gameStatePreview: null, gameStateList: [], selectedClue: {topic: null, offset: null}};
    },
    componentDidMount: function() {
        let socket = this.socket = io(window.location.hostname + ':' + 7171);

        socket.on('StateUpdated', function(data) {
            let newGameStateList = this.state.gameStateList.slice();
            newGameStateList.push(data);
            this.setState({ game: data, gameStateList: newGameStateList });
        }.bind(this));

        socket.on('StateList', function(list){
            let update = { gameStateList: list };
            if(list.length > 0) {
                update.game = list[list.length - 1];
            }
            this.setState(update);
        }.bind(this));

        socket.emit('GetStateList');
    },
    handleSessionOpened: function() {
        this.socket.emit('OpenSession');
        this.setState(this.getInitialState(), function(){
            this.commitGameState();
        });
    },
    handleClueSelected: function(clue) {
        this.setState({ selectedClue: clue });
    },
    handleClueStateUpdated: function(newClueState) {
        this.commitGameState({ clueDefinition: this.state.game.clueDefinition, clueState: newClueState });
    },
    handleSnapshotSelected: function(snapshot) {
        this.setState({ gameStatePreview: snapshot === this.state.gameStateList.length - 1 ? null : this.state.gameStateList[snapshot] });
    },
    handleSnapshotAccepted: function() {
        this.commitGameState(this.state.gameStatePreview);
    },
    commitGameState: function(state) {
        this.socket.emit('UpdateState', state ? state : this.state.game);
    },
    render: function() {
        let game = this.state.game,
            isPreviewing = !!this.state.gameStatePreview,
            visibleGame = isPreviewing ? this.state.gameStatePreview : game;

        return (
            <div className={"console rows" + (!this.props.enabled ? ' disabled' : '')}>
                <div className="cols">
                    <Jeopardy clueDefinition={visibleGame.clueDefinition}
                              clueState={visibleGame.clueState}
                              selectable={true}
                              clueSelected={this.handleClueSelected} />

                    <Controls clueDefinition={visibleGame.clueDefinition}
                              clueState={visibleGame.clueState}
                              enabled={!isPreviewing}
                              selectedClue={this.state.selectedClue}
                              clueStateUpdated={this.handleClueStateUpdated}
                              sessionOpened={this.handleSessionOpened} />
                </div>

                <Timeline gameStateList={this.state.gameStateList}
                          snapshopSelected={this.handleSnapshotSelected}
                          snapshotAccepted={this.handleSnapshotAccepted} />
            </div>
        );
    },
    componentDidUpdate: function(){
    }
});
