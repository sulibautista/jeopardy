"use strict";
import React from 'react';
import clueStateTools from '../clue-state-tools.js';


export default React.createClass({
    propTypes: {
        clueDefinition: clueStateTools.clueDefinitionType,
        activeClue: clueStateTools.clueLocatorType
    },
    render: function() {
        let clue = this.props.activeClue,
            state = clue ? 'shown' : 'hidden',
            txt = clue ? this.props.clueDefinition[clue.topic].clues[clue.offset][clue.clue.activeState] : '';

        return (
            <div className={"overlord-screen " + state + (clue ? " active-" + clue.clue.activeState : "")}>
                <div className="overlord-inner">
                    {txt}
                    <div className="overlord-meta rows">
                        <span className="overlord-question">{this.props.clueDefinition[clue.topic].clues[clue.offset].question}</span>
                        <span className="overlord-answer">{this.props.clueDefinition[clue.topic].clues[clue.offset].answer}</span>
                    </div>
                </div>
            </div>
        );
    }
});
