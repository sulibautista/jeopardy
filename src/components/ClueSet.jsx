"use strict";
import React from "react";
import Clue from "./Clue.jsx";
import clueStateTools from '../clue-state-tools.js';

export default React.createClass({
    propTypes: {
        clues: React.PropTypes.arrayOf(clueStateTools.clueType).isRequired,
        topic: React.PropTypes.string.isRequired,
        topicIndex: React.PropTypes.number.isRequired,
        selectable: React.PropTypes.bool.isRequired,
        clueSelected: React.PropTypes.func.isRequired,
        selectedClue: React.PropTypes.number
    },
    handleClueSelected: function(offset) {
        this.props.clueSelected(this.props.topicIndex, offset);
    },
    render: function() {
        let clues = this.props.clues.map(function(clue, i){
            return (
               <Clue data={clue} key={i}
                     clueOffset={i}
                     selectable={this.props.selectable}
                     selected={this.handleClueSelected}
                     isSelected={this.props.selectedClue === i} />
            );
        }, this);

        return (
            <div className="clue-set">
                <span className="clue-topic">{this.props.topic}</span>
                {clues}
            </div>
        );
    }
});
