"use strict";
import React from "react";
import _ from "lodash";
import clueStateTools from '../clue-state-tools.js';

export default React.createClass({
    propTypes: {
        data: clueStateTools.clueType,
        clueOffset: React.PropTypes.number.isRequired,
        selectable: React.PropTypes.bool.isRequired,
        isSelected: React.PropTypes.bool.isRequired,
        selected: React.PropTypes.func
    },
    handleClick: function(){
        if(this.props.selectable) {
            this.props.selected(this.props.clueOffset);
        }
    },
    render: function() {
        let data = this.props.data,
            classes = ['clue', data.state];

        if(this.props.isSelected){
            classes.push('selected');
        }

        if(data.activeState) {
            classes.push('active-' + data.activeState);
        }

        return (
            <span className={_.compact(classes).join(' ')} onClick={this.handleClick}>
                <span className="clue-prize">{data.prize}</span>
                <span className="clue-question">{data.question}</span>
                <span className="clue-answer">{data.answer}</span>
            </span>
        );
    }
});
