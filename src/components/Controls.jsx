"use strict";
import React from 'react';
import _ion from 'ion-sound'; // eslint-disable-line no-unused-vars
import _ from 'lodash';
import clueStateTools from '../clue-state-tools.js';

const sounds = {
    ok: ['ok0', 'ok1', 'ok2', 'ok3', 'ok4', 'ok5'],
    bad: ['fail'],
    claps: ['solo']
};

/* global ion */
ion.sound({
    sounds: _(sounds)
        .map(function(v){
            return v.map(function(s){ return { name: s }; });
        }).flatten().value(),
    volume: .6,
    path: "sound/",
    preload: true
});

export default React.createClass({
    propTypes: {
        clueDefinition: clueStateTools.clueDefinitionType,
        clueState: clueStateTools.clueStateType,
        enabled: React.PropTypes.bool.isRequired,
        selectedClue: React.PropTypes.shape({
            topic: React.PropTypes.number,
            offset: React.PropTypes.number,
            clue: React.PropTypes.shape(clueStateTools.clueStateBase)
        }).isRequired,
        clueStateUpdated: React.PropTypes.func.isRequired,
        sessionOpened: React.PropTypes.func.isRequired
    },
    getInitialState: function() {
        return { multPercentage: 100 };
    },
    componentDidMount() {
        this.shorcuts = {
            '1': [this.handleTeamSelected.bind(this, 1)],
            '2': [this.handleTeamSelected.bind(this, 2)],
            '3': [this.handleTeamSelected.bind(this, 0)],
            'q': ['ActivateQ'],
            'w': [this.handlePlaySound.bind(this, 'ok'), 'ActivateA'],
            'e': ['Cash'],
            'r': [this.handlePlaySound.bind(this, 'bad')],
            'a': ['ActivateA'],
            's': [this.handlePlaySound.bind(this, 'ok')],
            'f': [this.handlePlaySound.bind(this, 'bad'), 'ActivateA'],
            'z': ['Reset']
        };
        document.addEventListener("keyup", this.handleShortcuts);
    },
    componentWillUnmount() {
        document.removeEventListener("keyup", this.handleShortcuts);
    },
    handleShortcuts: function(e) {
        if(this.isDisabled() || !e.target || e.target.tagName === 'INPUT' || e.ctrlKey || e.altKey || e.shiftKey){
            return;
        }

        let cmd = this.shorcuts[String.fromCharCode(e.which).toLowerCase()];

        if(cmd){
            cmd.forEach(function(task){
                if(_.isFunction(task)) {
                    task();
                } else {
                    this['handle' + task]();
                }
            }, this);
        }
    },
    handleActivateQ: function() {
        this.setActiveState('question');
    },
    handleActivateA: function() {
        this.setActiveState('answer');
    },
    setActiveState: function(substate) {
        let sel = this.getSelectedClue(),
            active = this.getActiveClue(),
            newState;

        if(!this.isActiveStateTransitionable(sel, substate)){
            return;
        }

        if(active) {
            if(this.sameClue(sel, active)){
                this.updateState(this.transformClue(sel, { activeState: substate }));
                return;
            } else {
                newState = this.transformClue(active, { state: active.clue.prevState });
            }
        }

        this.updateState(this.transformClue(sel, {
            state: 'active',
            activeState: substate
        }, newState));
    },
    handleTeamSelected: function(teamId) {
        let sel = this.getSelectedClue();

        if(sel.clue.state === 'active' || sel.clue.state === 'cashed') {
            this.updateState(this.transformClue(sel, { team: teamId }));
        }
    },
    handleCash: function() {
        let sel = this.getSelectedClue();

        if(sel.clue.state !== 'active' || _.isNumber(sel.clue.team)) {
            this.updateState(this.transformClue(sel, {
                state: 'cashed'
            }));
        }
    },
    handleCancel: function() {
        let sel = this.getSelectedClue();

        if(sel.clue.state === 'active') {
            this.updateState(this.transformClue(sel, { state: sel.clue.prevState }));
        }
    },
    handleMultChange: function(e) {
        this.setState({ multPercentage: parseInt(e.target.value.replace(/\D/g, ''), 10) });
    },
    handleMultKeyUp: function(e) {
        if(e.which === 13) { // Enter
            e.stopPropagation();
            this.commitMultChange();
        }
    },
    handleMultBlur: function() {
        this.commitMultChange();
    },
    commitMultChange: function() {
        this.updateState(this.transformClue(this.getSelectedClue(), {
            bountyMult: this.state.multPercentage / 100
        }));
    },
    handleReset: function() {
        this.updateState(this.transformClue(this.getSelectedClue(), { state: 'hidden' }));
    },
    handlePlaySound: function(name) {
        ion.sound.play(_.sample(sounds[name]));
    },
    handleNewSession: function() {
        this.props.sessionOpened();
    },
    getActiveClue: function(){
        return clueStateTools.getActive(this.props.clueState);
    },
    getSelectedClue: function() {
        let topic = this.props.selectedClue.topic,
            offset = this.props.selectedClue.offset;
        return {
            topic, offset,
            clue: this.props.clueState[topic][offset]
        };
    },
    sameClue: function(a, b){
        return a.topic === b.topic && a.offset === b.offset;
    },
    transformClue: function(clue, mods, newState) {
        if(!newState) {
            // Clone the two dimensional array, we dont want to clone deep because we only change one clue state at
            // a time
            newState = this.props.clueState.map(function(clueSet){
                return clueSet.slice();
            });
        }

        let oldState = clue.clue.state,
            newClue = _.extend({}, clue.clue, mods);

        if(!_.isEqual(newClue, clue.clue)) {
            newState[clue.topic][clue.offset] = newClue;

            if(mods.state) {
                newState[clue.topic][clue.offset].prevState = oldState === 'active' || oldState === mods.state ? 'hidden' : oldState;
            }

            return newState;
        }

        return null;
    },
    updateState: function(newState) {
        if(newState) {
            this.props.clueStateUpdated(newState);
        }
    },
    hasSelectedClue: function() {
        return _.isNumber(this.props.selectedClue.topic) && _.isNumber(this.props.selectedClue.offset);
    },
    isDisabled: function() {
        return !this.props.enabled || !this.hasSelectedClue();
    },
    isActiveStateTransitionable: function(clue, activeState) {
        return !!this.props.clueDefinition[clue.topic].clues[clue.offset][activeState];
    },
    componentWillReceiveProps: function(nextProps) {
        let selClue = _.isNumber(nextProps.selectedClue.topic) && _.isNumber(nextProps.selectedClue.offset),
            activeClue = selClue && nextProps.clueState[nextProps.selectedClue.topic][nextProps.selectedClue.offset],
            multValue = activeClue ? Math.floor(activeClue.bountyMult * 100) : 100;

        if(_.isNumber(multValue) && this.state.multPercentage !== multValue) {
            this.setState({ multPercentage: multValue });
        }
    },
    render: function(){
        let dis = this.isDisabled(),
            clue = this.hasSelectedClue() && this.getSelectedClue().clue,
            disAnswer = dis || !this.isActiveStateTransitionable(this.props.selectedClue, 'answer'),
            team = clue && clue.team;

        return (
            <div className="controls cols">
                <div className="rows">
                    <button disabled={dis} className="control-activateQ" onClick={this.handleActivateQ}>Pregunta</button>
                    <div className="cols">
                        <button disabled={dis} onClick={this.handleTeamSelected.bind(this, 1)} className="control-team control-team1" data-pushed={team === 1}>Team 1</button>
                        <button disabled={dis} onClick={this.handleTeamSelected.bind(this, 2)} className="control-team control-team2" data-pushed={team === 2}>Team 2</button>
                        <button disabled={dis} onClick={this.handleTeamSelected.bind(this, 0)} className="control-team control-no-team" data-pushed={team !== 1 && team !== 2}>X</button>
                    </div>
                    <button disabled={disAnswer} onClick={this.handleActivateA} className="control-activateA">Respuesta</button>
                    <div className="cols">
                        <button disabled={dis} onClick={this.handleCash} className="control-cash">Cash</button>
                        <button disabled={dis} onClick={this.handleCancel} className="control-cancel">Cancel</button>
                    </div>
                    <button disabled={dis} onClick={this.handleReset} className="control-reset">Reset</button>
                    <button className="control-spacer"></button>
                    <button className="control-open-session" onClick={this.handleNewSession}>New Session</button>
                </div>
                <div className="rows">
                    <button disabled={dis} onClick={this.handlePlaySound.bind(this, 'ok')} className="control-right">Y</button>
                    <button disabled={dis} onClick={this.handlePlaySound.bind(this, 'bad')} className="control-wrong">N</button>
                    <button disabled={dis} onClick={this.handlePlaySound.bind(this, 'claps')} className="control-sound">♫</button>
                    <button className="control-spacer"></button>
                    <input disabled={dis} ref="bountyMultInput" type="text" size="6" value={this.state.multPercentage}
                           onChange={this.handleMultChange} onBlur={this.handleMultBlur} onKeyUp={this.handleMultKeyUp} />
                    <button className="control-spacer"></button>
                </div>
            </div>
        );
    }
});
