"use strict";
import React from "react";
import DisplayController from "./components/DisplayController.jsx";

React.render(
    <DisplayController />,
    document.getElementById('content')
);
