"use strict";
import React from "react";
import ConsoleController from "./components/ConsoleController.jsx";

React.render(
    <ConsoleController />,
    document.getElementById('content')
);
