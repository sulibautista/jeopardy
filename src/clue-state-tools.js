"use strict";
import React from "react";
import _ from "lodash";

let clueDefBase = {
    question: React.PropTypes.string.isRequired,
    prize: React.PropTypes.number.isRequired,
    answer: React.PropTypes.string
};

let clueStateBase = {
    state: React.PropTypes.oneOf(['hidden', 'active', 'cashed']).isRequired,
    prevState: React.PropTypes.oneOf(['hidden', 'cashed']).isRequired,
    bountyMult: React.PropTypes.number.isRequired,
    activeState: React.PropTypes.oneOf(['question', 'answer']),
    team: React.PropTypes.number
};

export default {
    clueStateBase,

    clueType: React.PropTypes.shape(_.extend({}, clueDefBase, clueStateBase)).isRequired,

    clueDefinitionType: React.PropTypes.arrayOf(
        React.PropTypes.shape({
            topic: React.PropTypes.string.isRequired,
            clues: React.PropTypes.arrayOf(
                React.PropTypes.shape(clueDefBase).isRequired
            ).isRequired
        }).isRequired
    ).isRequired,

    clueStateType: React.PropTypes.arrayOf(
        React.PropTypes.arrayOf(
            React.PropTypes.shape(clueStateBase).isRequired
        ).isRequired
    ).isRequired,

    clueLocatorType: React.PropTypes.shape({
        topic: React.PropTypes.number.isRequired,
        offset: React.PropTypes.number.isRequired,
        clue: React.PropTypes.shape(clueStateBase).isRequired
    }).isRequired,

    forEach: function(clueState, fn){
        for(var i = 0; i < clueState.length; ++i){
            for(var k = 0; k < clueState[i].length; ++k) {
                if(fn(clueState[i][k], i, k) === false) { return; }
            }
        }
    },
    getActive: function(clueState) {
        for(var i = 0; i < clueState.length; ++i){
            for(var k = 0; k < clueState[i].length; ++k) {
                if(clueState[i][k].state === 'active') {
                    return {
                        topic: i,
                        offset: k,
                        clue: clueState[i][k]
                    };
                }
            }
        }
        return null;
    }
};
