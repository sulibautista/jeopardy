jest.dontMock('../src/components/Clue.jsx');

import _ from 'lodash';
import React from 'react/addons';

let Clue = require('../src/components/Clue.jsx');

let TestUtils = React.addons.TestUtils,
    clueFixture = {
        question: "q",
        prize: 200,
        answer: "a",
        state: "hidden",
        prevState: "hidden",
        bountyMult: 1,
        activeState: null,
        team: 1
    };

describe('Clue', function(){
    it('outputs the correct class given the clue state', function() {
        let clueHidden = TestUtils.renderIntoDocument(
                <Clue data={_.extend({}, clueFixture, { state: 'hidden' })} clueOffset={1} selectable={false} isSelected={false} />
            ),
            clueActive = TestUtils.renderIntoDocument(
                <Clue data={_.extend({}, clueFixture, { state: 'active' })} clueOffset={2} selectable={false} isSelected={true} />
            ),
            clueCashed = TestUtils.renderIntoDocument(
                <Clue data={_.extend({}, clueFixture, { state: 'cashed' })} clueOffset={3} selectable={true} isSelected={false} />
            );

        TestUtils.findRenderedDOMComponentWithClass(clueHidden, 'hidden');
        TestUtils.findRenderedDOMComponentWithClass(clueActive, 'active');
        TestUtils.findRenderedDOMComponentWithClass(clueCashed, 'cashed');
    });

    it('renders the correct text properties [prize, question and answer]', function(){
        let clue = TestUtils.renderIntoDocument(
                <Clue data={clueFixture} clueOffset={1} selectable={false} selected={function(){}} isSelected={false} />
            );

        expect(TestUtils.findRenderedDOMComponentWithClass(clue, 'clue-prize').getDOMNode().textContent).toBe(clueFixture.prize.toString());
        expect(TestUtils.findRenderedDOMComponentWithClass(clue, 'clue-question').getDOMNode().textContent).toBe(clueFixture.question);
        expect(TestUtils.findRenderedDOMComponentWithClass(clue, 'clue-answer').getDOMNode().textContent).toBe(clueFixture.answer);
    });

    it('calls the given "selected" prop function on click', function() {
        let callback = jest.genMockFunction(),
            clueOffset = 3,
            clue = TestUtils.renderIntoDocument(
                <Clue data={clueFixture} clueOffset={clueOffset} selectable={true} selected={callback} isSelected={false} />
            ),
            btn = TestUtils.findRenderedDOMComponentWithClass(clue, 'clue');

        TestUtils.Simulate.click(btn);
        expect(callback.mock.calls[0][0]).toBe(clueOffset);
    });
});
